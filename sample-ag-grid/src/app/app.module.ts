import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AgGridComponent } from './components/grid-component/ag-grid.component';
import 'ag-grid-enterprise';
import { GridWithApplyCancelButtonComponent } from './components/grid-with-apply-cancel-button/grid-with-apply-cancel-button.component';
import { GridWithCustomButtonsComponent } from './components/grid-with-custom-buttons/grid-with-custom-buttons.component';

@NgModule({
  declarations: [
    AppComponent,
    AgGridComponent,
    GridWithApplyCancelButtonComponent,
    GridWithCustomButtonsComponent
  ],
  imports: [
    AgGridModule.withComponents([]),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
