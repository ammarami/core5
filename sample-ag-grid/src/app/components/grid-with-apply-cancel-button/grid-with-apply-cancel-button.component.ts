import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid-with-apply-cancel-button',
  templateUrl: './grid-with-apply-cancel-button.component.html',
  styleUrls: ['./grid-with-apply-cancel-button.component.css']
})
export class GridWithApplyCancelButtonComponent{

  gridApi:any;
  private gridColumnApi:any;

  private columnDefs:any;
  private defaultColDef:any;
  private rowData: [];

  constructor(private http: HttpClient) {
    this.columnDefs = [
      {
        field: 'athlete',
        filter: 'agTextColumnFilter',
        filterParams: {
          buttons: ['reset', 'apply'],
        },
      },
      {
        field: 'age',
        maxWidth: 100,
        filter: 'agNumberColumnFilter',
        filterParams: {
          buttons: ['apply', 'reset'],
          closeOnApply: true,
        },
      },
      {
        field: 'country',
        filter: 'agSetColumnFilter',
        filterParams: {
          buttons: ['clear', 'apply'],
        },
      },
      {
        field: 'year',
        filter: 'agSetColumnFilter',
        filterParams: {
          buttons: ['apply', 'cancel'],
          closeOnApply: true,
        },
        maxWidth: 100,
      },
      { field: 'sport' },
      {
        field: 'gold',
        filter: 'agNumberColumnFilter',
      },
      {
        field: 'silver',
        filter: 'agNumberColumnFilter',
      },
      {
        field: 'bronze',
        filter: 'agNumberColumnFilter',
      },
      {
        field: 'total',
        filter: 'agNumberColumnFilter',
      },
    ];
    this.defaultColDef = {
      flex: 1,
      minWidth: 150,
      filter: true,
    };
  }

  onGridReady(params:any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.http
      .get('https://www.ag-grid.com/example-assets/olympic-winners.json')
      .subscribe((data) => {
        this.rowData = data;
      });
  }

}
