import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid-with-custom-buttons',
  templateUrl: './grid-with-custom-buttons.component.html',
  styleUrls: ['./grid-with-custom-buttons.component.css']
})
export class GridWithCustomButtonsComponent implements OnInit {
  private gridApi;
  private gridColumnApi;

  private columnDefs;
  private datasource;
  private components;
  private defaultColDef;
  private rowSelection;
  private rowModelType;
  private maxBlocksInCache;
  private infiniteInitialRowCount;
  private maxConcurrentDatasourceRequests;
  private getRowNodeId;
  private getRowStyle;
  private rowData: [];

  constructor() {
    this.columnDefs = [
      {
        headerName: 'Item ID',
        field: 'id',
        valueGetter: 'node.id',
        cellRenderer: 'loadingRenderer',
      },
      { field: 'make' },
      { field: 'model' },
      {
        field: 'price',
        valueFormatter: valueFormatter,
      },
    ];
    this.datasource = {
      rowCount: null,
      getRows: function (params) {
        console.log('asking for ' + params.startRow + ' to ' + params.endRow);
        setTimeout(function () {
          var rowsThisPage = allOfTheData.slice(params.startRow, params.endRow);
          for (var i = 0; i < rowsThisPage.length; i++) {
            var item = rowsThisPage[i];
            var itemCopy = JSON.parse(JSON.stringify(item));
            rowsThisPage[i] = itemCopy;
          }
          var lastRow = -1;
          if (allOfTheData.length <= params.endRow) {
            lastRow = allOfTheData.length;
          }
          params.successCallback(rowsThisPage, lastRow);
        }, 500);
      },
    };
    this.components = {
      loadingRenderer: function (params) {
        if (params.value !== undefined) {
          return params.value;
        } else {
          return "<img src=\"https://www.ag-grid.com/example-assets/loading.gif\">";
        }
      },
    };
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
    this.rowModelType = 'infinite';
    this.maxBlocksInCache = 2;
    this.infiniteInitialRowCount = 500;
    this.maxConcurrentDatasourceRequests = 2;
    this.getRowNodeId = function (item) {
      return item.id.toString();
    };
    this.getRowStyle = function (params) {
      if (params.data && params.data.make === 'Honda') {
        return { fontWeight: 'bold' };
      } else {
        return null;
      }
    };
  }

  insertItemsAt2AndRefresh(count) {
    insertItemsAt2(count);
    var maxRowFound = this.gridApi.isLastRowIndexKnown();
    if (maxRowFound) {
      var rowCount = this.gridApi.getInfiniteRowCount();
      this.gridApi.setRowCount(rowCount + count);
    }
    this.gridApi.refreshInfiniteCache();
  }

  removeItem(start, limit) {
    allOfTheData.splice(start, limit);
    this.gridApi.refreshInfiniteCache();
  }

  refreshCache() {
    this.gridApi.refreshInfiniteCache();
  }

  purgeCache() {
    this.gridApi.purgeInfiniteCache();
  }

  setRowCountTo200() {
    this.gridApi.setRowCount(200, false);
  }

  rowsAndMaxFound() {
    console.log(
      'getInfiniteRowCount() => ' + this.gridApi.getInfiniteRowCount()
    );
    console.log(
      'isLastRowIndexKnown() => ' + this.gridApi.isLastRowIndexKnown()
    );
  }

  setPricesHigh() {
    allOfTheData.forEach(function (dataItem) {
      dataItem.price = Math.round(55500 + 400 * (0.5 + Math.random()));
    });
  }

  setPricesLow() {
    allOfTheData.forEach(function (dataItem) {
      dataItem.price = Math.round(1000 + 100 * (0.5 + Math.random()));
    });
  }

  printCacheState() {
    console.log('*** Cache State ***');
    console.log(this.gridApi.getCacheBlockState());
  }

  jumpTo500() {
    if (this.gridApi.getInfiniteRowCount() < 501) {
      this.gridApi.setRowCount(501, false);
    }
    this.gridApi.ensureIndexVisible(500);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    sequenceId = 1;
    allOfTheData = [];
    for (var i = 0; i < 1000; i++) {
      allOfTheData.push(createRowData(sequenceId++));
    }
  }
}

var valueFormatter = function (params) {
  if (typeof params.value === 'number') {
    return '\xA3' + params.value.toLocaleString();
  } else {
    return params.value;
  }
};
var sequenceId;
var allOfTheData;
function createRowData(id) {
  var makes = ['Toyota', 'Ford', 'Porsche', 'Chevy', 'Honda', 'Nissan'];
  var models = [
    'Cruze',
    'Celica',
    'Mondeo',
    'Boxter',
    'Genesis',
    'Accord',
    'Taurus',
  ];
  return {
    id: id,
    make: makes[id % makes.length],
    model: models[id % models.length],
    price: 72000,
  };
}
function insertItemsAt2(count) {
  var newDataItems = [];
  for (var i = 0; i < count; i++) {
    var newItem = createRowData(sequenceId++);
    allOfTheData.splice(2, 0, newItem);
    newDataItems.push(newItem);
  }
  return newDataItems;

}
