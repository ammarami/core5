import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomWindow } from './custom.window';

@Component({
  selector: 'app-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: []
})
export class AgGridComponent implements OnInit {
  private paginationPageSize;
  declare window: CustomWindow;
  gridApi: any;
  gridColumnApi: any;
  @Output() isGetFormClicked = new EventEmitter();
  columnDefs: any;
  defaultColDef: any;
  sideBar: any;
  autoGroupColumnDef: any;
  rowSelection: any;
  rowGroupPanelShow;
  pivotPanelShow;
  rowData: Observable<any[]>;
  pinnedBottomRowData: any;

  constructor(private http: HttpClient) {
    this.columnDefs = [
      { field: 'athlete', sort: 'desc', pinned: 'left' },
      {
        field: 'latinText',
        width: 350,
        wrapText: true,
      },
      { field: 'age', sortable: true, filter: true },
      { field: 'country' },
      { field: 'sport' },
      { field: 'year' },
      { field: 'date' },
      { field: 'gold' },
      { field: 'silver' },
      { field: 'bronze' },
      { field: 'total' },
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      width: 150,
      enableRowGroup: true,
      enablePivot: true,
      enableValue: true,
    };
    this.paginationPageSize = 10;
    this.autoGroupColumnDef = {
      headerName: 'Athlete',
      field: 'athlete',
      minWidth: 250,
      cellRenderer: 'agGroupCellRenderer',
      cellRendererParams: { checkbox: true },
    };
    this.rowSelection = 'multiple';
    this.sideBar = { toolPanels: ['columns'] };
    this.rowGroupPanelShow = 'always';
    this.pivotPanelShow = 'always';
    this.pinnedBottomRowData = createData(1, 'Bottom');
  }

  ngOnInit() {
  }

  // onQuickFilterChanged() {
  //   this.gridApi.setQuickFilter(document.getElementById('quickFilter').value);
  // }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.http
      .get('https://www.ag-grid.com/example-assets/olympic-winners.json')
      .subscribe((data: any) => {

        data.forEach(function (dataItem: any) {
          dataItem.latinText = latinText;
        });

        var differentHeights = [40, 80, 120, 200];
        data.forEach(function (dataItem: any, index: number) {
          dataItem.rowHeight = differentHeights[index % 4];
        });
        this.rowData = data;
      });
  }

  getRowHeight(params: any) {
    return params.data.rowHeight;
  }
  addForm() {
    this.isGetFormClicked.emit({ id: '0' });
  }

}

function createData(count: any, prefix: any) {
  var result = [];
  for (var i = 0; i < count; i++) {
    result.push({
      athlete: prefix + ' Athlete ' + i,
      age: prefix + ' Age ' + i,
      country: prefix + ' Country ' + i,
      year: prefix + ' Year ' + i,
      date: prefix + ' Date ' + i,
      sport: prefix + ' Sport ' + i,
    });
  }
  return result;
}

var latinText =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

